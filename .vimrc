set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#rc()

" Let vundle manage itself:
Plugin 'VundleVim/Vundle.vim'

" Just a shitload of color schemes.
Plugin 'flazz/vim-colorschemes'

" NERDtree - File explorer
Plugin 'scrooloose/nerdtree'

" Fuzzy finder -- absolutely must have.
Plugin 'kien/ctrlp.vim'

" Support for easily toggling comments.
Plugin 'tpope/vim-commentary'

" More sensible character/construct matching
Plugin 'vim-scripts/matchit.zip'

" Shows git diff in the 'gutter' (sign column)
Plugin 'airblade/vim-gitgutter'

" Powerful vim integration
Plugin 'tpope/vim-fugitive'

" Powerful lightweight status bar
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" Highlight (and strip) whitespace characters
Plugin 'ntpeters/vim-better-whitespace'

" Displays tags in a window ordered by scope
Plugin 'majutsushi/tagbar'

" Support for golang
Plugin 'fatih/vim-go'

" Support for Cheetah templates
Plugin 'wting/cheetah.vim'

" Fast CtrlP matcher for python
Plugin 'FelikZ/ctrlp-py-matcher'

" Syntax checking
Plugin 'scrooloose/syntastic'

" We have to turn this stuff back on if we want all of our features.
filetype plugin indent on " Filetype auto-detection
syntax on " Syntax highlighting

" Show line numbers
set number

" Always show status line
set laststatus=2

set background=dark
let g:solarized_termcolors=256
let g:solarized_termtrans=1
colorscheme solarized

set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab " use spaces instead of tabs.
set smarttab " let's tab key insert 'tab stops', and bksp deletes tabs.
set shiftround " tab / shifting moves to closest tabstop.
set autoindent " Match indents on new lines.
set smartindent " Intellegently dedent / indent new lines based on rules.

" We have VCS -- we don't need this stuff.
set nobackup " We have vcs, we don't need backups.
set nowritebackup " We have vcs, we don't need backups.
set noswapfile " They're just annoying. Who likes them?

" don't nag me when hiding buffers
set hidden " allow me to have buffers with unsaved changes.
set autoread " when a file has changed on disk, just load it. Don't ask.

" Make search more sane
set ignorecase " case insensitive search
set smartcase " If there are uppercase letters, become case-sensitive.
set incsearch " live incremental searching
set showmatch " live match highlighting
set hlsearch " highlight matches
set gdefault " use the `g` flag by default.

" Make <F2> toggle NERDTree plugin
map <F2> :NERDTreeToggle<CR>

" Make <F8> toggle TagBar plugin
map <F8> :TagbarToggle<CR>

" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme = 'solarized'
let g:airline#extensions#hunks#enabled=0
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#syntastic#enabled = 1

" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

:nnoremap <C-l> :bnext<CR>
:nnoremap <C-h> :bprevious<CR>

" Show vertical column at position 80
set cc=80

" Strip whitespace on save for certain file types
autocmd FileType c,cpp,go,java,javascript,python autocmd BufWritePre <buffer> StripWhitespace

let g:ctrlp_use_caching = 1
let g:ctrlp_clear_cache_on_exit = 0
let g:ctrlp_user_command = 'ag %s -i --nocolor --nogroup --hidden
      \ --ignore .git
      \ --ignore .svn
      \ --ignore .hg
      \ --ignore .DS_Store
      \ --ignore "**/*.pyc"
      \ -g ""'
let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }

" Syntastic configuration
let g:syntastic_python_checkers = ['flake8']
let g:syntastic_python_pep8_args='--ignore=E501'

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

set tags=./tags;/

